## Experimental plan

### LSH

#### Basic experiments

1. Three-layer setup
   1. Cos/Fastfood (for kernel)
   2. Tanh (for "soft" hashing)
   3. Two-input metric cost 
2. Three-layer setup
   1. Linear (to reduce parameters)
   1. Cos (for kernel)
   2. Tanh (for "soft" hashing)
   3. Two-input metric cost 

#### Datasets

1. MNIST
   * Regular MNIST
     - will need to be balanced to account for (dis)similarity
       There are 10 classes in this set -- assuming that they are equally balanced, there are nine times as many dissimilar pairs as similar pairs
   * Expanded MNIST sets, but same problem applies

#### Comparison

1. Existing work
   * List here
2. Same networks as above, except don't learn kernel
   * I.e. keep W_lr_scale = 0.0
   * Use a few known static kernels like Gaussian, Laplacian, Cauchy
     - Use these as initializations later

